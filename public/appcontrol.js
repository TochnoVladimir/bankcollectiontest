var module = angular.module('myapp', ['ngResource', 'ui.utils','ngCookies']);

//Контроллер пользователей
module.factory('User', function ($resource) {
    return $resource('1/BankDivision/:idPerf/User', {idPerf: '@idPerf'});
})
    .controller('UserController', function ($scope, $cookies, $timeout ,$rootScope, User) {
        $scope.test;
        var favoriteCookie = $cookies.myFavorite;
        if(favoriteCookie!=undefined){
            $scope.test=favoriteCookie;}
        $scope.isAvtorisation=false;
        $scope.isRegisrtation=false;
        $scope.updateFlagUser=false;
        $rootScope.sessionUser;
        $scope.roles = [
            {name: 'ghost'},
            {name: 'user'},
            {name: 'admin'},
            {name: 'umbelivble'}
        ];

        $scope.playSign  = function() {
            var user = new User();
            var user2 = new User();
            user.login = $scope.login;
            user.password = $scope.password;
            user.$save(toka()).then(function (data) {
                user2= data;
                if(user2.role == null ){
                    alert("Неверные данные");
                }else{
                    $scope.tokenFunc(user2.login);
                    $cookies.myFavorite = user2.login;
                }
                });
            return false;
        };
        $scope.tokenFunc = function (logToken) {
            var user = new User();
            user.login= logToken
            user.$save(check()).then(function (data) {
                if(data.role!=null) {
                    $scope.isAvtorisation = true;
                    $rootScope.sessionUser = data;
                }
            });
        }

        $scope.signOut = function () {
            $scope.isAvtorisation = false;
            $rootScope.sessionUser = null;
            $scope.login = "";
            $scope.password = "";
            $cookies.myFavorite=undefined;
        }

        $scope.onRegistration = function () {
            $scope.isRegisrtation = true;
        };
        $scope.backSign = function () {
            $scope.isRegisrtation = false;
        }
        $scope.playReg = function () {
            var user = new User();
            if(compareLogin($scope.loginReg)) {
                user.name = $scope.nameReg;
                user.login = $scope.loginReg;
                user.password = $scope.passwordReg;
                user.role = "ghost";
                user.$save(url(), function () {
                    update();
                });
                alert("Сохранено");
                $scope.isRegisrtation = false;
            }else alert("Такой логин уже есть")
        }
        var url = function () {
            return {idPerf: $scope.selectedRowUser || '1'};
        };
        var check = function () {
            return {idPerf: 'check'};
        };
        var toka = function () {
            return {idPerf: 'toka'};
        };
        var update = function () {
            $scope.users = User.query(url());
            $rootScope.userRequests = User.query(url());
        };
        $scope.selectUser = function (selectUser) {
            $scope.selectedRowUser = selectUser.id;
            $scope.UserSelect = selectUser;
            $scope.deleteButtonDisabledUser = false;
        };
        $scope.addUser = function () {
            if(compareLogin($scope.loginUser)||$scope.updateFlagUser) {
                var user = new User();
                user.name = $scope.nameUser;
                user.login = $scope.loginUser;
                user.role = $scope.roleUser.name;
                user.password = $scope.paswwordUser;
                if($scope.updateFlagUser){
                user.id = $scope.UserSelect.id;
            }
                user.$save(url(), function () {
                    $scope.nameUser = "";
                    $scope.loginUser = "";
                   $scope.roleUser = null;
                    $scope.paswwordUser = "";
                    $scope.updateFlagUser=false;
                    update();
                });
            }else alert("Такой логин уже есть")
        };
        $scope.updateUser = function () {
            if($scope.selectedRowUser) {
                $scope.nameUser=$scope.UserSelect.name;
                $scope.loginUser=$scope.UserSelect.login;
                $scope.roleUser=$scope.UserSelect.role;
                $scope.paswwordUser=$scope.UserSelect.password;
                $scope.updateFlagUser = true;
            }
        }

        $scope.deleteUser = function () {
            var user = new User();
            user.id = $scope.UserSelect.id;
            user.$delete(url(), function () {
                update();
                $scope.updateFlagUser=false;
                $scope.deleteButtonDisabledUser = true;
            });
        };
        $scope.refreshUser = function () {
            update();
        }
        var compareLogin = function (loga) {
            for(var i=0;i<$scope.users.length;i++){
                if($scope.users[i].login == loga){
                    return false;
                }
            }
            return true;
        }
        $rootScope.flagUserRole = function (rol) {
            switch(rol){
                case 0:
                    if($rootScope.sessionUser.role == "ghost"){
                        return true
                    };
                case 1:
                    if($rootScope.sessionUser.role == "user"){
                        return true
                    };
                case 2:
                    if($rootScope.sessionUser.role == "admin"){
                        return true
                    };
                case 3:
                    if($rootScope.sessionUser.role == "umbelivble"){
                        return true
                    }else break;
            }
            //if($scope.roles.name.indexOf($rootScope.sessionUser)<rol){
            //    return true;
            //}else return false;
        }
        $scope.tokenFunc($scope.test);
        update();
    });

//Контроллер подразделений банка
module.factory('BankDivision', function ($resource) {
    return $resource(':idPerf/BankDivision', {idPerf: '@idPerf'});
})
    .controller('BankDivisionController', function ($scope ,$rootScope, BankDivision) {
        var url = function () {
            return {idPerf: $scope.selectedRowBankDivision || '1'};
        };
        var update = function () {
            $scope.BankDivisions = BankDivision.query(url());
        };
        $scope.selectBankDivision = function (selectBankDivision) {
            $scope.selectedRowBankDivision = selectBankDivision.id;
            $scope.BankDivisionSelect = selectBankDivision;
            $scope.deleteButtonDisabled = false;
        };
        $scope.addBankDivision = function () {
            var bankDivision = new BankDivision();
            bankDivision.nameBankDivision = $scope.nameBankDivision;
            bankDivision.$save(url(), function () {
                $scope.nameBankDivision = "";
                update();
            });
        };
        $scope.deleteBankDivision = function () {
            var bankDivision = new BankDivision();
            bankDivision.id = $scope.BankDivisionSelect.id;
            bankDivision.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabled = true;
            });
        };
        $scope.refreshBankDivision = function () {
            update();
        }
        update();
    });

//Контроллер типа  заявки
module.factory('TypeRequest', function ($resource) {
    return $resource('1/BankDivision/:idPerf/TypeRequest', {idPerf: '@idPerf'});
})
    .controller('TypeRequestController', function ($scope ,$rootScope, TypeRequest) {
        var url = function () {
            return {idPerf: $scope.selectedRowTypeRequest || '1'};
        };
        var update = function () {
            $scope.TypeRequests = TypeRequest.query(url());
            $rootScope.setTypeRequest = TypeRequest.query(url());
        };
        $scope.selectTypeRequest = function (selectTypeRequest) {
            $scope.selectedRowTypeRequest = selectTypeRequest.id;
            $scope.TypeRequestSelect = selectTypeRequest;
            $scope.deleteButtonDisabledTypeRequest = false;
        };
        $scope.addTypeRequest = function () {
            var typeRequest = new TypeRequest();
            typeRequest.nameType = $scope.nameType;
            typeRequest.$save(url(), function () {
                $scope.nameType = "";
                update();
            });
        };
        $scope.deleteTypeRequest = function () {
            var typeRequest = new TypeRequest();
            typeRequest.id = $scope.TypeRequestSelect.id;
            typeRequest.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabledTypeRequest = true;
            });
        };
        $scope.refreshTypeRequest = function () {
            update();
        }
        update();
    });

//Контроллер заявок инкассации
module.factory('RequestEncashment', function ($resource) {
    return $resource('1/BankDivision/:idPerf/RequestEncashment', {idPerf: '@idPerf'});
})
    .controller('RequestEncashmentController', function ($scope ,$rootScope, RequestEncashment) {
        $rootScope.flagDelObject;
        $scope.updateFlagRequest = false;
        $scope.flagStatusRequest = true;
        $rootScope.setTypeRequest={}
        $scope.statuss = [
            {name: 'СОЗДАН'},
            {name: 'ПОДПИСАН'},
            {name: 'УДАЛЁН'},
            {name: 'ИСПОЛНЕН'}
        ];
        var url = function () {
            return {idPerf: $scope.selectedRowRequestEncashment || '1'};
        };
        var urlUser = function () {
            if($scope.flagUserRole(2)){
                return {idPerf: 'all'};
            }else {
                return {idPerf: $rootScope.sessionUser.id};
            }
        };
        $rootScope.fullUpdate=function () {
            update();
        };
        var update = function () {
                $scope.RequestEncashments = RequestEncashment.query(urlUser());
        };

        $scope.selectRequestEncashment = function (selectRequestEncashment) {
            $scope.selectedRowRequestEncashment = selectRequestEncashment.id;
            $scope.RequestEncashmentSelect = selectRequestEncashment;
            $rootScope.fullUpdate();
            $scope.deleteButtonDisabledRequestEncashment = false;
            if(selectRequestEncashment.statusRequest ==  $scope.statuss[0].name){
                $scope.flagStatusRequest = false;
            }else $scope.flagStatusRequest = true;
        };
        $scope.addRequestEncashment = function () {
            var requestEncashment = new RequestEncashment();
            requestEncashment.typeRequest = $scope.typerRequest
            requestEncashment.nameCompany = $scope.nameCompany;
            requestEncashment.dateRequest = $scope.dateRequest;
            requestEncashment.bik = $scope.bik;
            requestEncashment.numberRequest = $scope.numberRequest;
            requestEncashment.creditAccountNumber = $scope.creditAccountNumber;
            requestEncashment.statusRequest = $scope.statusRequest.name;
            requestEncashment.correspondentAccountNumber = $scope.correspondentAccountNumber;
            requestEncashment.bankDivision = $scope.bankvisions
            requestEncashment.bankName = $scope.bankName;
            requestEncashment.innKio = $scope.innKio;
            requestEncashment.swift = $scope.swift;
            requestEncashment.kpp = $scope.kpp;
            requestEncashment.otherRequesites = $scope.otherRequesites;
            requestEncashment.ogrn = $scope.ogrn;
            requestEncashment.nameAuthorizedOfficer = $scope.nameAuthorizedOfficer;
            requestEncashment.phoneAuthorizedOfficer = $scope.phoneAuthorizedOfficer;
            if($scope.flagUserRole(2)){
                requestEncashment.user = $scope.userRequest;
            }else{
                requestEncashment.user = $rootScope.sessionUser;
            }
            if($scope.updateFlagRequest){
                requestEncashment.id = $scope.selectedRowRequestEncashment;
            }
            requestEncashment.$save(url(), function () {
                update();
                $scope.selectedRowRequestEncashment = null;
                $scope.clearAddForm();
            });
        };
        $scope.clearAddForm = function(){
            $scope.typerRequest = null;
            $scope.nameCompany = null;
            $scope.dateRequest = null;
            $scope.bik = null;
            $scope.numberRequest = null;
            $scope.creditAccountNumber = null;
            $scope.statusRequest = null;
            $scope.correspondentAccountNumber = null;
            $scope.bankvisions = null;
            $scope.bankName = null;
            $scope.innKio = null;
            $scope.swift = null;
            $scope.kpp = null;
            $scope.otherRequesites = null;
            $scope.ogrn = null;
            $scope.nameAuthorizedOfficer = null;
            $scope.phoneAuthorizedOfficer = null;
            $scope.userRequest = null;
        }
        $scope.deleteRequestEncashment = function () {
            if($rootScope.flagDelObject){
                alert("Необходимо удалить все объекты перед удалением заявки")
            }else{
            var requestEncashment = new RequestEncashment();
            requestEncashment.id = $scope.RequestEncashmentSelect.id;
            $scope.nameTest = $scope.RequestEncashmentSelect.id;
            requestEncashment.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabledRequestEncashment = true;
                $scope.selectedRowRequestEncashment = null;
            });
            }
        };
        $scope.signRequest = function () {
            var requestEncashment = new RequestEncashment();
            requestEncashment = $scope.RequestEncashmentSelect;
            requestEncashment.statusRequest = $scope.statuss[1].name;
            requestEncashment.$save(url(), function () {
                update();
                $scope.deleteButtonDisabledRequestEncashment = true;
                $scope.selectedRowRequestEncashment = null;
            });
         };
        $scope.initListRequest = function () {
            $scope.updateFlagRequest = false;
            $scope.dateRequest = new Date();
            var str = "2-0000000";
            if($scope.RequestEncashments!='') {
                var str2 = String(Number($scope.RequestEncashments[$scope.RequestEncashments.length - 1].id) + 1);
            }else var str2 = "1";
            $scope.numberRequest = str.substr(0,str.length - str2.length) + str2 ;
            $scope.listTypeRequest = $rootScope.setTypeRequest;
            $scope.statusRequest = $scope.statuss[0];

        }
        $scope.searchItem = function (list, item) {
            for(var i=0;i< list.length;i++){
                if(list[i].id == item.id){
                    return i;
                }
        }}
        $scope.searchItemName = function (list, item) {
            for(var i=0;i< list.length;i++){
                if(list[i].name == item){
                    return i;
                }
            }}
        $scope.initUpdateRequest= function () {
            $scope.listTypeRequest = $rootScope.setTypeRequest;

            $scope.typerRequest = $scope.listTypeRequest[$scope.searchItem($scope.listTypeRequest, $scope.RequestEncashmentSelect.typeRequest)];
            $scope.bankvisions = $scope.BankDivisions[$scope.searchItem($scope.BankDivisions,$scope.RequestEncashmentSelect.bankDivision)];
            $scope.statusRequest =  $scope.statuss[$scope.searchItemName($scope.statuss, $scope.RequestEncashmentSelect.statusRequest)];

            $scope.dateRequest =new Date($scope.RequestEncashmentSelect.dateRequest);

            $scope.bik = $scope.RequestEncashmentSelect.bik;
            $scope.numberRequest = $scope.RequestEncashmentSelect.numberRequest;
            $scope.creditAccountNumber = $scope.RequestEncashmentSelect.creditAccountNumber;
            $scope.nameCompany = $scope.RequestEncashmentSelect.nameCompany;
            $scope.correspondentAccountNumber = $scope.RequestEncashmentSelect.correspondentAccountNumber;
            $scope.bankName = $scope.RequestEncashmentSelect.bankName;
            $scope.innKio = $scope.RequestEncashmentSelect.innKio;
            $scope.swift = $scope.RequestEncashmentSelect.swift;
            $scope.kpp = $scope.RequestEncashmentSelect.kpp;
            $scope.otherRequesites = $scope.RequestEncashmentSelect.otherRequesites;
            $scope.ogrn= $scope.RequestEncashmentSelect.ogrn;
            $scope.nameAuthorizedOfficer = $scope.RequestEncashmentSelect.nameAuthorizedOfficer;
            $scope.phoneAuthorizedOfficer = $scope.RequestEncashmentSelect.phoneAuthorizedOfficer;
            $scope.updateFlagRequest = true;

            $scope.userRequest = $rootScope.userRequests[$scope.searchItem($rootScope.userRequests,$scope.RequestEncashmentSelect.user)];
        }
        update();
        $scope.refreshRequestEncashment = function () {
            update();
        }
    });



//Контроллер объекты инкассации
module.factory('ObjectEncashment', function ($resource) {
    return $resource('1/BankDivision/:idoe/ObjectEncashment', {idoe: '@idoe'});
})
    .controller('ObjectEncashmentController', function ($scope,$rootScope, ObjectEncashment) {
        var url = function () {
            return {idoe: $scope.selectedRowObjectEncashment || '1'};
        };
        var urlwork = function () {
            return {idoe: $scope.selectedRowRequestEncashment || 'all'};
        };
        $rootScope.setCode={};
        $rootScope.setCurrency={};
        $rootScope.setPeriodicity={};
        $rootScope.setServicet={};
        var update = function () {
            $scope.ObjectEncashments = ObjectEncashment.query(urlwork(), function (data) {
                if(data == ""){
                    $rootScope.flagDelObject = false;
                }else  $rootScope.flagDelObject = true;
            });
        };
        $rootScope.fullUpdate = function () {
            update();
        };
        $scope.day = [
            {name: 'Понедельник'},
            {name: 'Вторник'},
            {name: 'Среда'},
            {name: 'Четверг'},
            {name: 'Пятница'},
            {name: 'Суббота'},
            {name: 'Воскресенье'}
        ];
        $scope.typeAddresss = [
            {name: '01'},
            {name: '02'},
            {name: '03'},
            {name: '04'},
            {name: '05'},
            {name: '06'},
            {name: '07'},
            {name: '08'}
        ];
        $scope.typeCitys = [
            {name: '01'},
            {name: '02'},
            {name: '03'},
            {name: '04'},
            {name: '05'},
            {name: '06'},
            {name: '07'},
            {name: '09'}
        ];
        $scope.midDate =  new Date();

        $scope.selectObjectEncashment = function (selectObjectEncashment) {
            $scope.selectedRowObjectEncashment = selectObjectEncashment.id;
            $scope.ObjectEncashmentSelect = selectObjectEncashment;
            $scope.deleteButtonDisabledObjectEncashment = false;
        };
        $scope.copyObjectEncashment = function () {
            if($scope.selectedRowRequestEncashment != null){
                var objectEncashment = new ObjectEncashment();
                objectEncashment = $scope.ObjectEncashmentSelect;
                objectEncashment.id = null;
                objectEncashment.$save(urlwork(), function () {
                    update();
                });
            }else alert("Нельзя сотворить это без исполнителя");
        };
        $scope.addObjectEncashment = function () {
            if($scope.selectedRowRequestEncashment != null){
                var objectEncashment = new ObjectEncashment();
                objectEncashment.timeStartEncashment = $scope.timeStartEncashment;
                objectEncashment.currencyDepositMethod = $scope.currencyDepositMethod;
                objectEncashment.periodicityRequest = $scope.PeriodicityRequest;
                objectEncashment.codeCash = $scope.obCodeCashob;
                objectEncashment.encashmentServices = $scope.encashmentServices;
                if($scope.days != undefined) {
                    objectEncashment.day = $scope.days.name;
                }
                objectEncashment.cashbox = $scope.cashbox;
                objectEncashment.objectBoss = $scope.objectBoss;
                objectEncashment.stroreOpeningDate = $scope.stroreOpeningDate;
                objectEncashment.typeAddress = $scope.typeAddress.name;
                objectEncashment.typeCity = $scope.typeCity.name;
                objectEncashment.nameCity = $scope.nameCity;
                objectEncashment.street = $scope.street;
                objectEncashment.nomberHouse = $scope.nomberHouse;
                objectEncashment.hoising = $scope.hoising;

                if ($scope.flagPeriodicity('Рабочие дни')){
                objectEncashment.workDayTimeStart = $scope.workDayTimeStart;
                objectEncashment.workDayTimeEnd = $scope.workDayTimeEnd;
                }
                if($scope.flagDay('Суббота')) {
                objectEncashment.saturdayTimeStart = $scope.saturdayTimeStart;
                objectEncashment.saturdayTimeEnd = $scope.saturdayTimeEnd;
                }
                if($scope.flagDay('Воскресенье')) {
                    objectEncashment.sundayTimeStart = $scope.sundayTimeStart;
                    objectEncashment.sundayTimeEnd = $scope.sundayTimeEnd;
                }
                if($scope.updateFlagObject){
                    objectEncashment.id = $scope.selectedRowObjectEncashment;
                }
                objectEncashment.$save(urlwork(), function () {
                    $scope.deleteButtonDisabledObjectEncashment = true;
                    $scope.selectedRowObjectEncashment = null;
                    $scope.clearAddObjectForm();
                    update();
                });
            }else alert("Нельзя сотворить это без исполнителя");
        };
        $scope.deleteObjectEncashment = function () {
            var objectEncashment = new ObjectEncashment();
            objectEncashment.id = $scope.ObjectEncashmentSelect.id;
            objectEncashment.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabledObjectEncashment = true;
                $scope.selectedRowObjectEncashment = null;
            });
        };
        $scope.initObject = function(){
            $scope.listCode = $rootScope.setCode;
            $scope.listCurrency = $rootScope.setCurrency;
            $scope.listPeriodicity = $rootScope.setPeriodicity;
            $scope.listServicet = $rootScope.setServicet;
            $scope.updateFlagObject = false;
        }
        $scope.initObjectUpdate = function() {
            $scope.listCode = $rootScope.setCode;
            $scope.listCurrency = $rootScope.setCurrency;
            $scope.listPeriodicity = $rootScope.setPeriodicity;
            $scope.listServicet = $rootScope.setServicet;

            $scope.timeStartEncashment = new Date($scope.ObjectEncashmentSelect.timeStartEncashment);
            $scope.stroreOpeningDate = new Date($scope.ObjectEncashmentSelect.stroreOpeningDate);

            $scope.currencyDepositMethod = $scope.listCurrency[$scope.searchItem($scope.listCurrency, $scope.ObjectEncashmentSelect.currencyDepositMethod)];
            $scope.PeriodicityRequest = $scope.listPeriodicity[$scope.searchItem($scope.listPeriodicity, $scope.ObjectEncashmentSelect.periodicityRequest)];
            $scope.obCodeCashob = $scope.listCode[$scope.searchItem($scope.listCode, $scope.ObjectEncashmentSelect.codeCash)];
            $scope.encashmentServices = $scope.listServicet[$scope.searchItem($scope.listServicet, $scope.ObjectEncashmentSelect.encashmentServices)];

            $scope.days = $scope.day[$scope.searchItemName($scope.day, $scope.ObjectEncashmentSelect.day)];
            $scope.typeAddress = $scope.typeAddresss[$scope.searchItemName($scope.typeAddresss, $scope.ObjectEncashmentSelect.typeAddress)];
            $scope.typeCity = $scope.typeCitys[$scope.searchItemName($scope.typeCitys, $scope.ObjectEncashmentSelect.typeCity)];

            $scope.cashbox = $scope.ObjectEncashmentSelect.cashbox;
            $scope.objectBoss = $scope.ObjectEncashmentSelect.objectBoss;
            $scope.nameCity = $scope.ObjectEncashmentSelect.nameCity;
            $scope.street = $scope.ObjectEncashmentSelect.street;
            $scope.nomberHouse = $scope.ObjectEncashmentSelect.nomberHouse;
            $scope.hoising = $scope.ObjectEncashmentSelect.hoising;
            $scope.updateFlagObject = true;

            if ($scope.flagPeriodicity('Рабочие дни')){
                $scope.workDayTimeStart = new Date($scope.ObjectEncashmentSelect.workDayTimeStart);
                $scope.workDayTimeEnd = new Date($scope.ObjectEncashmentSelect.workDayTimeEnd);
            }
            if($scope.flagDay('Суббота')) {
                $scope.saturdayTimeStart = new Date($scope.ObjectEncashmentSelect.saturdayTimeStart);
                $scope.saturdayTimeEnd = new Date($scope.ObjectEncashmentSelect.saturdayTimeEnd);
            }
            if($scope.flagDay('Воскресенье')) {
                $scope.sundayTimeStart = new Date($scope.ObjectEncashmentSelect.sundayTimeStart);
                $scope.sundayTimeEnd = new Date($scope.ObjectEncashmentSelect.sundayTimeEnd);
            }

        }
        $scope.clearAddObjectForm = function () {
            $scope.listCode = null;
            $scope.listCurrency = null;
            $scope.listPeriodicity = null;
            $scope.listServicet = null;
            $scope.timeStartEncashment = null;
            $scope.currencyDepositMethod = null;
            $scope.PeriodicityRequest = null;
            $scope.obCodeCashob = null;
            $scope.encashmentServices = null;
            $scope.days = null;
            $scope.cashbox = null;
            $scope.objectBoss = null;
            $scope.stroreOpeningDate = null;
            $scope.typeAddress = null;
            $scope.typeCity = null;
            $scope.nameCity = null;
            $scope.street = null;
            $scope.nomberHouse = null;
            $scope.hoising = null;

            $scope.workDayTimeStart = null;
            $scope.workDayTimeEnd = null;
            $scope.saturdayTimeStart = null;
            $scope.saturdayTimeEnd = null;
            $scope.sundayTimeStart = null;
            $scope.sundayTimeEnd = null;

        }
        $scope.refreshObject = function () {
            update();
        }
        $scope.flagLengthObject = function () {
            if($scope.ObjectEncashments!=""){
                if ($scope.ObjectEncashments.length>4){
                    return true;
                }else return false;
            }else return false;
        }
        $scope.flagPeriodicity = function (str) {
            if ($scope.PeriodicityRequest != undefined) {
                if ($scope.PeriodicityRequest.namePeriodicity == str) {
                    return true;
                } else return false;
            } else return false;
        }
        $scope.flagDay = function (str) {
            if ($scope.days != undefined) {
                if ($scope.days.name == str) {
                    return true;
                } else return false;
            } else return false;
        }

        $scope.testFunc = function () {
            alert($scope.workDayTimeStart)
        }
        update();
    });






//Контроллер кода валют
module.factory('CodeCash', function ($resource) {
    return $resource('1/BankDivision/:idPerf/CodeCash', {idPerf: '@idPerf'});
})
    .controller('CodeCashController', function ($scope,$rootScope, CodeCash) {
        var url = function () {
            return {idPerf: $scope.selectedRowCodeCash || '1'};
        };
        var update = function () {
            $scope.CodeCashs = CodeCash.query(url());
            $rootScope.setCode = CodeCash.query(url());
        };
        $scope.selectCodeCash = function (selectCodeCash) {
            $scope.selectedRowCodeCash = selectCodeCash.id;
            $scope.CodeCashSelect = selectCodeCash;
            $scope.deleteButtonDisabled = false;
        };
        $scope.addCodeCash = function () {
            var codeCash = new CodeCash();
            codeCash.nameCodeCash = $scope.nameCodeCash;
            codeCash.$save(url(), function () {
                $scope.nameCodeCash = "";
                update();
            });
        };
        $scope.deleteCodeCash = function () {
            var codeCash = new CodeCash();
            codeCash.id = $scope.CodeCashSelect.id;
            codeCash.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabled = true;
            });
        };
        $scope.refreshCodeCash = function () {
            update();
        }
        update();
    });

//Контроллер  метода сдачи валюты
module.factory('CurrencyDepositMethod', function ($resource) {
    return $resource('1/BankDivision/:idPerf/CurrencyDepositMethod', {idPerf: '@idPerf'});
})
    .controller('CurrencyDepositMethodController', function ($scope, $rootScope, CurrencyDepositMethod) {
        var url = function () {
            return {idPerf: $scope.selectedRowCurrencyDepositMethod || '1'};
        };
        var update = function () {
            $scope.CurrencyDepositMethods = CurrencyDepositMethod.query(url());
            $rootScope.setCurrency = CurrencyDepositMethod.query(url());
        };
        $scope.selectCurrencyDepositMethod = function (selectCurrencyDepositMethod) {
            $scope.selectedRowCurrencyDepositMethod = selectCurrencyDepositMethod.id;
            $scope.CurrencyDepositMethodSelect = selectCurrencyDepositMethod;
            $scope.deleteButtonDisabled = false;
        };
        $scope.addCurrencyDepositMethod = function () {
            var currencyDepositMethod = new CurrencyDepositMethod();
            currencyDepositMethod.nameMethod = $scope.nameMethod;
            currencyDepositMethod.$save(url(), function () {
                $scope.nameMethod = "";
                update();
            });
        };
        $scope.deleteCurrencyDepositMethod = function () {
            var currencyDepositMethod = new CurrencyDepositMethod();
            currencyDepositMethod.id = $scope.CurrencyDepositMethodSelect.id;
            currencyDepositMethod.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabled = true;
            });
        };
        $scope.refreshCurrencyDepositMethod = function () {
            update();
        }
        update();
    });

//Контроллер  услуг инкассации
module.factory('EncashmentServices', function ($resource) {
    return $resource('1/BankDivision/:idPerf/EncashmentServices', {idPerf: '@idPerf'});
})
    .controller('EncashmentServicesController', function ($scope, $rootScope, EncashmentServices) {
        var url = function () {
            return {idPerf: $scope.selectedRowEncashmentServices || '1'};
        };
        var update = function () {
            $scope.EncashmentServicess = EncashmentServices.query(url());
            $rootScope.setServicet =  EncashmentServices.query(url());
        };
        $scope.selectEncashmentServices = function (selectEncashmentServices) {
            $scope.selectedRowEncashmentServices = selectEncashmentServices.id;
            $scope.EncashmentServicesSelect = selectEncashmentServices;
            $scope.deleteButtonDisabled = false;
        };
        $scope.addEncashmentServices = function () {
            var encashmentServices = new EncashmentServices();
            encashmentServices.nameServices = $scope.nameServices;
            encashmentServices.$save(url(), function () {
                $scope.nameServices = "";
                update();
            });
        };
        $scope.deleteEncashmentServices = function () {
            var encashmentServices = new EncashmentServices();
            encashmentServices.id = $scope.EncashmentServicesSelect.id;
            encashmentServices.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabled = true;
            });
        };
        $scope.refreshEncashmentServices = function () {
            update();
        }
        update();
    });

//Контроллер переодичности
module.factory('PeriodicityRequest', function ($resource) {
    return $resource('1/BankDivision/:idPerf/PeriodicityRequest', {idPerf: '@idPerf'});
})
    .controller('PeriodicityRequestController', function ($scope, $rootScope, PeriodicityRequest) {
        var url = function () {
            return {idPerf: $scope.selectedRowPeriodicityRequest || '1'};
        };
        var update = function () {
            $scope.PeriodicityRequests = PeriodicityRequest.query(url());
            $rootScope.setPeriodicity = PeriodicityRequest.query(url());
        };

        $scope.selectPeriodicityRequest = function (selectPeriodicityRequest) {
            $scope.selectedRowPeriodicityRequest = selectPeriodicityRequest.id;
            $scope.PeriodicityRequestSelect = selectPeriodicityRequest;
            $scope.deleteButtonDisabled = false;
        };
        $scope.addPeriodicityRequest = function () {
            var periodicityRequest = new PeriodicityRequest();
            periodicityRequest.namePeriodicity = $scope.namePeriodicity;
            periodicityRequest.$save(url(), function () {
                $scope.namePeriodicity = "";
                update();
            });
        };
        $scope.deletePeriodicityRequest = function () {
            var periodicityRequest = new PeriodicityRequest();
            periodicityRequest.id = $scope.PeriodicityRequestSelect.id;
            periodicityRequest.$delete(url(), function () {
                update();
                $scope.deleteButtonDisabled = true;
            });
        };
        $scope.refreshPeriodicityRequest = function () {
            update();
        }
        update();
    });


