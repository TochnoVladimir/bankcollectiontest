package managers.repository;

import managers.models.CurrencyDepositMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface CurrencyDepositMethodRepository extends JpaRepository<CurrencyDepositMethod, Long> {
    Optional<CurrencyDepositMethod> findById(Long id);
}
