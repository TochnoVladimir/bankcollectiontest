package managers.repository;

import managers.models.BankDivision;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface BankDivisionRepository extends JpaRepository<BankDivision, Long> {
    Optional<BankDivision> findById(Long id);
}
