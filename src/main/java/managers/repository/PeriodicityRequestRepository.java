package managers.repository;

import managers.models.PeriodicityRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface PeriodicityRequestRepository extends JpaRepository<PeriodicityRequest, Long> {
    Optional<PeriodicityRequest> findById(Long id);
}
