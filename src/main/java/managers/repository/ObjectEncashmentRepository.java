package managers.repository;

import managers.models.ObjectEncashment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface ObjectEncashmentRepository extends JpaRepository<ObjectEncashment, Long> {
    Optional<ObjectEncashment> findById(Long id);
    Collection<ObjectEncashment> findByRequestEncashmentId(Long id);

}
