package managers.repository;

import managers.models.CodeCash;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface CodeCashRepository extends JpaRepository<CodeCash, Long> {
    Optional<CodeCash> findById(Long id);
}
