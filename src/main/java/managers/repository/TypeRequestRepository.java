package managers.repository;

import managers.models.TypeRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface TypeRequestRepository extends JpaRepository<TypeRequest,Long> {
    Optional<TypeRequest> findById(Long id);
}
