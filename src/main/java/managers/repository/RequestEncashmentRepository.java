package managers.repository;

import managers.models.RequestEncashment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface RequestEncashmentRepository extends JpaRepository<RequestEncashment, Long> {
    Optional<RequestEncashment> findById(Long id);
    Collection<RequestEncashment> findByUserId(Long id);
}
