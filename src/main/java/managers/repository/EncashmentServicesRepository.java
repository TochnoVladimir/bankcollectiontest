package managers.repository;

import managers.models.EncashmentServices;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface EncashmentServicesRepository extends JpaRepository<EncashmentServices, Long> {
    Optional<EncashmentServices> findById(Long id);
}
