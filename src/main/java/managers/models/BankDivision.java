package managers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BankDivision {
    @Id
    @GeneratedValue
    private Long id;
    private String nameBankDivision;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameBankDivision() {
        return nameBankDivision;
    }

    public void setNameBankDivision(String nameBankDivision) {
        this.nameBankDivision = nameBankDivision;
    }
}
