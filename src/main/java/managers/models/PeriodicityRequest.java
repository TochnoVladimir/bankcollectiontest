package managers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class PeriodicityRequest {
    @Id
    @GeneratedValue
    private Long id;
    private String namePeriodicity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamePeriodicity() {
        return namePeriodicity;
    }

    public void setNamePeriodicity(String namePeriodicity) {
        this.namePeriodicity = namePeriodicity;
    }
}
