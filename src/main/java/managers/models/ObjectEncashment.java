package managers.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ObjectEncashment {
    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    public RequestEncashment requestEncashment;


    @ManyToOne
    private CurrencyDepositMethod currencyDepositMethod;
    @ManyToOne
    private PeriodicityRequest periodicityRequest;
    @ManyToOne
    private CodeCash codeCash;
    @ManyToOne
    private EncashmentServices encashmentServices;

    private Date stroreOpeningDate;
    private Date timeStartEncashment;

    private Date workDayTimeStart;
    private Date workDayTimeEnd;
    private Date saturdayTimeStart;
    private Date saturdayTimeEnd;
    private Date sundayTimeStart;
    private Date sundayTimeEnd;

    private String day;
    private String cashbox;
    private String objectBoss;
    private String typeAddress;
    private String typeCity;
    private String nameCity;
    private String street;
    private String nomberHouse;
    private String hoising;

    public RequestEncashment getRequestEncashment() {
        return requestEncashment;
    }

    public void setRequestEncashment(RequestEncashment requestEncashment) {
        this.requestEncashment = requestEncashment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimeStartEncashment() {
        return timeStartEncashment;
    }

    public void setTimeStartEncashment(Date timeStartEncashment) {
        this.timeStartEncashment = timeStartEncashment;
    }

    public CurrencyDepositMethod getCurrencyDepositMethod() {
        return currencyDepositMethod;
    }

    public void setCurrencyDepositMethod(CurrencyDepositMethod currencyDepositMethod) {
        this.currencyDepositMethod = currencyDepositMethod;
    }

    public PeriodicityRequest getPeriodicityRequest() {
        return periodicityRequest;
    }

    public void setPeriodicityRequest(PeriodicityRequest periodicityRequest) {
        this.periodicityRequest = periodicityRequest;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCashbox() {
        return cashbox;
    }

    public void setCashbox(String cashbox) {
        this.cashbox = cashbox;
    }

    public CodeCash getCodeCash() {
        return codeCash;
    }

    public void setCodeCash(CodeCash codeCash) {
        this.codeCash = codeCash;
    }

    public String getObjectBoss() {
        return objectBoss;
    }

    public void setObjectBoss(String objectBoss) {
        this.objectBoss = objectBoss;
    }

    public Date getStroreOpeningDate() {
        return stroreOpeningDate;
    }

    public void setStroreOpeningDate(Date stroreOpeningDate) {
        this.stroreOpeningDate = stroreOpeningDate;
    }

    public String getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(String typeAddress) {
        this.typeAddress = typeAddress;
    }

    public String getTypeCity() {
        return typeCity;
    }

    public void setTypeCity(String typeCity) {
        this.typeCity = typeCity;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNomberHouse() {
        return nomberHouse;
    }

    public void setNomberHouse(String nomberHouse) {
        this.nomberHouse = nomberHouse;
    }

    public String getHoising() {
        return hoising;
    }

    public void setHoising(String hoising) {
        this.hoising = hoising;
    }

    public Date getWorkDayTimeStart() {
        return workDayTimeStart;
    }

    public void setWorkDayTimeStart(Date workDayTimeStart) {
        this.workDayTimeStart = workDayTimeStart;
    }

    public Date getWorkDayTimeEnd() {
        return workDayTimeEnd;
    }

    public void setWorkDayTimeEnd(Date workDayTimeEnd) {
        this.workDayTimeEnd = workDayTimeEnd;
    }

    public Date getSaturdayTimeStart() {
        return saturdayTimeStart;
    }

    public void setSaturdayTimeStart(Date saturdayTimeStart) {
        this.saturdayTimeStart = saturdayTimeStart;
    }

    public Date getSaturdayTimeEnd() {
        return saturdayTimeEnd;
    }

    public void setSaturdayTimeEnd(Date saturdayTimeEnd) {
        this.saturdayTimeEnd = saturdayTimeEnd;
    }

    public Date getSundayTimeStart() {
        return sundayTimeStart;
    }

    public void setSundayTimeStart(Date sundayTimeStart) {
        this.sundayTimeStart = sundayTimeStart;
    }

    public Date getSundayTimeEnd() {
        return sundayTimeEnd;
    }

    public void setSundayTimeEnd(Date sundayTimeEnd) {
        this.sundayTimeEnd = sundayTimeEnd;
    }

    public EncashmentServices getEncashmentServices() {
        return encashmentServices;
    }

    public void setEncashmentServices(EncashmentServices encashmentServices) {
        this.encashmentServices = encashmentServices;
    }
}
