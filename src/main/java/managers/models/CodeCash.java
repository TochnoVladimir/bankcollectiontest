package managers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CodeCash {
    @Id
    @GeneratedValue
    private Long id;
    private String nameCodeCash;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCodeCash() {
        return nameCodeCash;
    }

    public void setNameCodeCash(String nameCodeCash) {
        this.nameCodeCash = nameCodeCash;
    }
}
