package managers.models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class RequestEncashment {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private TypeRequest typeRequest;

    @ManyToOne
    private BankDivision bankDivision;

    @ManyToOne
    private User user;

    private String statusRequest;
    private Date dateRequest;
    private String numberRequest;
    private String innKio;
    private String kpp;
    private String nameCompany;
    private String ogrn;
    private String nameAuthorizedOfficer;
    private String phoneAuthorizedOfficer;
    private String creditAccountNumber;
    private String bik;
    private String correspondentAccountNumber;
    private String bankName;
    private String swift;
    private String otherRequesites;

    @OneToMany(mappedBy = "requestEncashment")
    private Set<ObjectEncashment> blockObject = new HashSet<ObjectEncashment>();

    public String getNumberRequest() {
        return numberRequest;
    }

    public void setNumberRequest(String numberRequest) {
        this.numberRequest = numberRequest;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeRequest getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(TypeRequest typeRequest) {
        this.typeRequest = typeRequest;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    public String getStatusRequest() {
        return statusRequest;
    }

    public void setStatusRequest(String statusRequest) {
        this.statusRequest = statusRequest;
    }

    public BankDivision getBankDivision() {
        return bankDivision;
    }

    public void setBankDivision(BankDivision bankDivision) {
        this.bankDivision = bankDivision;
    }

    public String getInnKio() {
        return innKio;
    }

    public void setInnKio(String innKio) {
        this.innKio = innKio;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public String getNameAuthorizedOfficer() {
        return nameAuthorizedOfficer;
    }

    public void setNameAuthorizedOfficer(String nameAuthorizedOfficer) {
        this.nameAuthorizedOfficer = nameAuthorizedOfficer;
    }

    public String getPhoneAuthorizedOfficer() {
        return phoneAuthorizedOfficer;
    }

    public void setPhoneAuthorizedOfficer(String phoneAuthorizedOfficer) {
        this.phoneAuthorizedOfficer = phoneAuthorizedOfficer;
    }

    public String getCreditAccountNumber() {
        return creditAccountNumber;
    }

    public void setCreditAccountNumber(String creditAccountNumber) {
        this.creditAccountNumber = creditAccountNumber;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getCorrespondentAccountNumber() {
        return correspondentAccountNumber;
    }

    public void setCorrespondentAccountNumber(String correspondentAccountNumber) {
        this.correspondentAccountNumber = correspondentAccountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOtherRequesites() {
        return otherRequesites;
    }

    public void setOtherRequesites(String otherRequesites) {
        this.otherRequesites = otherRequesites;
    }

    public Set<ObjectEncashment> getBlockObject() {
        return blockObject;
    }

    public void setBlockObject(Set<ObjectEncashment> blockObject) {
        this.blockObject = blockObject;
    }
}
