package managers;

import managers.models.*;
import managers.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


@RestController
@RequestMapping(value = "{id}/BankDivision")
public class RestServiceController {
    @Autowired
    private BankDivisionRepository bankDivisionRepository;
    @Autowired
    private CodeCashRepository codeCashRepository;
    @Autowired
    private CurrencyDepositMethodRepository currencyDepositMethodRepository;
    @Autowired
    private EncashmentServicesRepository encashmentServicesRepository;
    @Autowired
    private PeriodicityRequestRepository periodicityRequestRepository;
    @Autowired
    private TypeRequestRepository typeRequestRepository;
    @Autowired
    private RequestEncashmentRepository requestEncashmentRepository;
    @Autowired
    private ObjectEncashmentRepository objectEncashmentRepository;
    @Autowired
    private UserRepository userRepository;
    private Map<String, User> tokenRepositoriy = new HashMap<>();

    //Блок функций для выбора  банка
//    @RequestMapping(value = "{id}/BankDivision")
    @RequestMapping
    Collection<BankDivision> readBankDivision() {
        System.out.println("BankDivision");
        return bankDivisionRepository.findAll();
    };
//    @RequestMapping(value = "{id}/BankDivision",method = RequestMethod.POST)
    @RequestMapping(method = RequestMethod.POST)
    public void addBankDivision(@RequestBody BankDivision bankDivision) {
        System.out.println("add "+ bankDivision.getNameBankDivision());
        bankDivisionRepository.save(bankDivision);
    };
    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteBankDivision(@PathVariable String id) {
        System.out.println("удалить bankdivision ");
        Long idBankDivision = new Long(id);
        bankDivisionRepository.delete(idBankDivision);
    };

    //Блок юзера
    @RequestMapping(value = "{idus}/User")
    Collection<User> readUser() {
        System.out.println("User");
        return userRepository.findAll();
    };

    @RequestMapping(value = "toka/User",method = RequestMethod.POST)
    User checkUser(@RequestBody User user) {
        User uzver;
        System.out.println(user);
        uzver = userRepository.findByLogin(user.getLogin()).orElseThrow(() ->
                new UserNotFoundException(user.getLogin()));
        System.out.println("проверено");
        if(uzver.getPassword().equals(user.getPassword())) {
            System.out.println("пароли равны");
            System.out.println(uzver);
            Random rand = new Random();
            String token = user.getLogin()+ ":(" + user.getLogin()+":"+rand.nextInt(9999)+")";
            System.out.println(token);

            tokenRepositoriy.put(token,uzver);
            User userToken = new User();
            userToken.setLogin(token);
            userToken.setRole("1");
            System.out.println(userToken);
            return userToken;
        }else {
            System.out.println("пароли не равны");
            return user;
        }
    };

    @RequestMapping(value = "check/User",method = RequestMethod.POST)
    User tokenUser(@RequestBody User user) {
        User uzver;
        uzver=tokenRepositoriy.get(user.getLogin());
        System.out.println(uzver);
        return uzver;
    };

    @RequestMapping(value = "1/User",method = RequestMethod.POST)
    public void addUser(@RequestBody User user) {
        System.out.println(user.getLogin());
        userRepository.save(user);
    };
    @RequestMapping(value = "{idus}/User",method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String idus) {
        System.out.println("удалить User");
        Long idUser = new Long(idus);
        userRepository.delete(idUser);
    };

     //Блок функций для объектов инкассации
    @RequestMapping(value = "{idoe}/ObjectEncashment",method = RequestMethod.GET)
    Collection<ObjectEncashment> readObjectEncashment(@PathVariable String idoe) {
        if ("all".equals(idoe))
            return objectEncashmentRepository.findAll();
        Long idReq = new Long(idoe);
        System.out.println("ObjectEncashment   " + "idoe");
        return objectEncashmentRepository.findByRequestEncashmentId(idReq);
    };
    @RequestMapping(value = "{idoe}/ObjectEncashment",method = RequestMethod.POST)
    public void addObjectEncashment(@PathVariable String idoe,@RequestBody ObjectEncashment objectEncashment) {
        RequestEncashment requestEncashment;
        Long id = new Long(idoe);
        requestEncashment = requestEncashmentRepository.findById(id).orElseThrow(() ->
                new RequestEncashmentNotFoundException(idoe));
        objectEncashment.setRequestEncashment(requestEncashment);
        System.out.println("add" + objectEncashment.getNameCity());
        objectEncashmentRepository.save(objectEncashment);
    };
    @RequestMapping(value = "{idoe}/ObjectEncashment",method = RequestMethod.DELETE)
    public void deleteObjectEncashment(@PathVariable String idoe) {
        System.out.println("удалить ObjectEncashment");
        Long idObjectEncashment = new Long(idoe);
        objectEncashmentRepository.delete(idObjectEncashment);
    };

    //Блок функций для выбора  кода валюты
    @RequestMapping(value = "{idcc}/CodeCash")
    Collection<CodeCash> readCodeCash() {
        System.out.println("CodeCash");
        return codeCashRepository.findAll();
    };
    @RequestMapping(value = "{idcc}/CodeCash",method = RequestMethod.POST)
    public void addCodeCash(@RequestBody CodeCash codeCash) {
        System.out.println(codeCash.getNameCodeCash());
        codeCashRepository.save(codeCash);
    };
    @RequestMapping(value = "{idcc}/CodeCash",method = RequestMethod.DELETE)
    public void deleteCodeCash(@PathVariable String idcc) {
        System.out.println("удалить CodeCash");
        Long idCodeCash = new Long(idcc);
        codeCashRepository.delete(idCodeCash);
    };

    //Блок функций для выбора  метода сдачи
    @RequestMapping(value = "{idcu}/CurrencyDepositMethod")
    Collection<CurrencyDepositMethod> readCurrencyDepositMethod() {
        System.out.println("CurrencyDepositMethod");
        return currencyDepositMethodRepository.findAll();
    };
    @RequestMapping(value = "{idcu}/CurrencyDepositMethod",method = RequestMethod.POST)
    public void addCurrencyDepositMethod(@RequestBody CurrencyDepositMethod сurrencyDepositMethod) {
        System.out.println(сurrencyDepositMethod.getNameMethod());
        currencyDepositMethodRepository.save(сurrencyDepositMethod);
    };
    @RequestMapping(value = "{idcu}/CurrencyDepositMethod",method = RequestMethod.DELETE)
    public void deleteCurrencyDepositMethod(@PathVariable String idcu) {
        System.out.println("удалить CurrencyDepositMethod");
        Long idСurrencyDepositMethod = new Long(idcu);
        currencyDepositMethodRepository.delete(idСurrencyDepositMethod);
    };

    //Блок функция для выбора  услуг инкассации
    @RequestMapping(value = "{idc}/EncashmentServices")
    Collection<EncashmentServices> readEncashmentServices() {
        System.out.println("EncashmentServices");
        return encashmentServicesRepository.findAll();
    };
    @RequestMapping(value = "{idc}/EncashmentServices",method = RequestMethod.POST)
    public void addEncashmentServices(@RequestBody EncashmentServices encashmentServices) {
        System.out.println(encashmentServices.getNameServices());
        encashmentServicesRepository.save(encashmentServices);
    };
    @RequestMapping(value = "{idc}/EncashmentServices",method = RequestMethod.DELETE)
    public void deleteEncashmentServices(@PathVariable String idc) {
        System.out.println("удалить EncashmentServices");
        Long idEncashmentServices = new Long(idc);
        encashmentServicesRepository.delete(idEncashmentServices);
    };

    //Блок функций для выбора  периодичности
    @RequestMapping(value = "{idpe}/PeriodicityRequest")
    Collection<PeriodicityRequest> readPeriodicityRequest() {
        System.out.println("PeriodicityRequest");
        return periodicityRequestRepository.findAll();
    };
    @RequestMapping(value = "{idpe}/PeriodicityRequest",method = RequestMethod.POST)
    public void addPeriodicityRequest(@RequestBody PeriodicityRequest periodicityRequest) {
        System.out.println(periodicityRequest.getNamePeriodicity());
        periodicityRequestRepository.save(periodicityRequest);
    };
    @RequestMapping(value = "{idpe}/PeriodicityRequest",method = RequestMethod.DELETE)
    public void deletePeriodicityRequest(@PathVariable String idpe) {
        System.out.println("удалить PeriodicityRequest");
        Long idPeriodicityRequest = new Long(idpe);
        periodicityRequestRepository.delete(idPeriodicityRequest);
    };

    //Блок типа заявки
    @RequestMapping(value = "{idty}/TypeRequest")
    Collection<TypeRequest> readTypeRequest() {
        System.out.println("TypeRequest");
        return typeRequestRepository.findAll();
    };
    @RequestMapping(value = "{idty}/TypeRequest",method = RequestMethod.POST)
    public void addTypeRequest(@RequestBody TypeRequest typeRequest) {
        System.out.println(typeRequest.getNameType());
        typeRequestRepository.save(typeRequest);
    };
    @RequestMapping(value = "{idty}/TypeRequest",method = RequestMethod.DELETE)
    public void deleteTypeRequest(@PathVariable String idty) {
        System.out.println("удалить TypeRequest");
        Long idTypeRequest = new Long(idty);
        typeRequestRepository.delete(idTypeRequest);
    };

    //Блок функций заявок инкассации
    @RequestMapping(value = "{idre}/RequestEncashment")
    Collection<RequestEncashment> readRequestEncashment(@PathVariable String idre) {
        System.out.println("RequestEncashment");
        if("all".equals(idre)) {
            System.out.println("RequestEncashment для всех");
            return requestEncashmentRepository.findAll();
        }else{
            Long idRequestEncashment = new Long(idre);
            System.out.println("RequestEncashment для пользователя " +idRequestEncashment);
            return requestEncashmentRepository.findByUserId(idRequestEncashment);
        }
    };
    @RequestMapping(value = "{idre}/RequestEncashment",method = RequestMethod.POST)
    public void addRequestEncashment(@RequestBody RequestEncashment requestEncashment) {
        System.out.println(requestEncashment.getNameCompany());
        requestEncashmentRepository.save(requestEncashment);
    };
    @RequestMapping(value = "{idre}/RequestEncashment",method = RequestMethod.DELETE)
    public void deleteRequestEncashment(@PathVariable String idre) {
        System.out.println("удалить RequestEncashment");
        Long idRequestEncashment = new Long(idre);
        requestEncashmentRepository.delete(idRequestEncashment);
    };

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class RequestEncashmentNotFoundException extends RuntimeException {
        public RequestEncashmentNotFoundException(String id) {
            super("could not find RequestEncashment  id='" + id + "'");
        }
    }
    @ResponseStatus(HttpStatus.NOT_FOUND)
    class UserNotFoundException extends RuntimeException {
        public UserNotFoundException(String id) {
            super("Не найден логин '" + id + "'");
        }
    }
}
