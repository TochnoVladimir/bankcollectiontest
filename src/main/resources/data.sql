insert ignore into periodicity_request (id, name_periodicity) values (1,'Ежедневно');
insert ignore into periodicity_request (id, name_periodicity) values (2,'Рабочие дни');
insert ignore into periodicity_request (id, name_periodicity) values (3,'Через день');
insert ignore into periodicity_request (id, name_periodicity) values (4,'День недели');
insert ignore into periodicity_request (id, name_periodicity) values (5,'По заявке');
insert ignore into periodicity_request (id, name_periodicity) values (6,'По звонку');

insert ignore into currency_deposit_method (id, name_method) values (1,'По объявлению на взнос наличными');
insert ignore into currency_deposit_method (id, name_method) values (2,'В инкассаторских сумках');
insert ignore into currency_deposit_method (id, name_method) values (3,'Через службу инкассации');

insert ignore into bank_division (id, name_bank_division) values (1,'подразделение №1');
insert ignore into bank_division (id, name_bank_division) values (2,'подразделение №2');

insert ignore into code_cash (id, name_code_cash) values (1,'byn');
insert ignore into code_cash (id, name_code_cash) values (2,'byr');

insert ignore into encashment_services (id, name_services) values (1,'Инкассация денежной наличности');
insert ignore into encashment_services (id, name_services) values (2,'Прием и зачисление/перечисление денежной наличности');
insert ignore into encashment_services (id, name_services) values (3,'Доставка денежной наличности');
insert ignore into encashment_services (id, name_services) values (4,'Доставка банкнот/монет в обмен на банкноты/монеты другого номинала');
insert ignore into encashment_services (id, name_services) values (5,'Внесение изменений в действующий договор');

insert ignore into type_request (id, name_type) values (1,'Инкассация');
insert ignore into type_request (id, name_type) values (2,'Заявка на внесение изменений в действующий договор (добавление объекта)');
insert ignore into type_request (id, name_type) values (3,'Заявка на внесение изменений в действующий договор (исключение объекта)');
insert ignore into type_request (id, name_type) values (4,'Заявка на внесение изменений в действующий договор (изменение условий по объектам)');
insert ignore into type_request (id, name_type) values (5,'Заявка на внесение изменений в действующий договор (изменение реквизитов договора)');

insert ignore into user (id, login, password, name, role) values (1,'admin', 'admin', 'admin', 'admin');
insert ignore into user (id, login, password, name, role) values (2,'user', 'user', 'user', 'user');
